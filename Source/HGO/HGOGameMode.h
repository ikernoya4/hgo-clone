// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HGOGameMode.generated.h"

UCLASS(minimalapi)
class AHGOGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHGOGameMode();
};



