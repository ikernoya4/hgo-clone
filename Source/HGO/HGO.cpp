// Copyright Epic Games, Inc. All Rights Reserved.

#include "HGO.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HGO, "HGO" );

DEFINE_LOG_CATEGORY(LogHGO)
 