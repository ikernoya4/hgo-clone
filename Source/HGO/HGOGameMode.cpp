// Copyright Epic Games, Inc. All Rights Reserved.

#include "HGOGameMode.h"
#include "HGOPlayerController.h"
#include "HGOCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHGOGameMode::AHGOGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AHGOPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}