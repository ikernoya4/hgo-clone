// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainPlayerController.h"
#include "Protagonist.generated.h"

UCLASS()
class HGO_API AProtagonist : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AProtagonist();
private:
	bool canSlide=true;
	
	float initialGroundFriction;
	float initialBrakeDeceleration;
	float initialWalkSpeed;
	float initialCrouchSpeed;
	
protected:
	bool bIsJumping = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	class USpringArmComponent* springArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* camera;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)	
	bool bIsPlayerCrouched = false;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)	
	bool bCanSlide = true;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bShouldPlayerRoll = false;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bIsSliding=false;
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	float slideForce = 1000.0f;
	
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	float turnRate = 45;
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	float lookUpRate = 25;
	
	virtual void BeginPlay() override;

	//Movement Functions
	void CheckStartPlayerRoll();
	void EndPlayerRoll();
	
	void CheckCrouch();
	void CheckJump();

	void initMovementVariables();
	void ResetMovementVariables();
	void SetSlidingVariables();
	void CheckSlideStart();
	void SlideUpdate();
	
	void TurnRate(float value);
	void LookUpRate(float value);
public:	
	// Called every frames
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
