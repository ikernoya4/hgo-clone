// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerController.h"

void AMainPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();
	if (!_bHasInitializedInput) {
		InputComponent->BindAxis("MoveRight", this, &AMainPlayerController::MoveRight);
		InputComponent->BindAxis("MoveForward", this, &AMainPlayerController::MoveForward);

		_bHasInitializedInput = true;
	}

}

void AMainPlayerController::MoveRight(float value) {
	if (value != 0) {

	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw,0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	 
	GetPawn()->AddMovementInput(Direction, value);

	}
}

void AMainPlayerController::MoveForward(float value) {
	if (value) {

	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

	GetPawn()->AddMovementInput(Direction, value);

	}
}

