// Fill out your copyright notice in the Description page of Project Settings.


#include "Protagonist.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFrameWork/CharacterMovementComponent.h"

// Sets default values
AProtagonist::AProtagonist()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	springArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	springArm->SetupAttachment(RootComponent);
	springArm->TargetArmLength = 300.0f;
	springArm->bUsePawnControlRotation = true;

	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	camera->SetupAttachment(springArm, USpringArmComponent::SocketName); 
	camera->bUsePawnControlRotation = false;
	 
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0,540,0);

	bIsJumping = false;
	
}

// Called when the game starts or when spawned
void AProtagonist::BeginPlay()
{
	Super::BeginPlay();
	initMovementVariables();
}

// Called every frame
void AProtagonist::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SlideUpdate();
	if (bIsJumping) {
		Jump();
	}
	if(bIsPlayerCrouched && !GetCharacterMovement()->IsFalling()){
		Crouch();
	}
	else
	{
		UnCrouch();
	}
	
}

// Called to bind functionality to input
void AProtagonist::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Turn", this, &AProtagonist::TurnRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AProtagonist::LookUpRate);
	PlayerInputComponent->BindAxis("TurnRate", this, &AProtagonist::TurnRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AProtagonist::LookUpRate);
	PlayerInputComponent->BindAction("Jump",IE_Pressed ,this, &AProtagonist::CheckJump);
	PlayerInputComponent->BindAction("Jump",IE_Released ,this, &AProtagonist::CheckJump);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AProtagonist::CheckCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AProtagonist::CheckStartPlayerRoll);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AProtagonist::EndPlayerRoll);

}

void AProtagonist::CheckStartPlayerRoll()
{	
	if(GetCharacterMovement()->IsFalling() && !bShouldPlayerRoll)
		bShouldPlayerRoll=true;
}
void AProtagonist::EndPlayerRoll()
{
	bShouldPlayerRoll=false;
}


void AProtagonist::CheckCrouch()
{
	if(!GetCharacterMovement()->IsFalling())
	{
		if(bIsPlayerCrouched) bIsPlayerCrouched = false;
		else{
			bIsPlayerCrouched = true;
			CheckSlideStart();
		}
	}
	
}

void AProtagonist::CheckJump() {
	if (bIsJumping) bIsJumping = false;
	else bIsJumping = true;
}

void AProtagonist::initMovementVariables()
{
	initialGroundFriction = GetCharacterMovement()->GroundFriction;
	initialBrakeDeceleration = GetCharacterMovement()->BrakingDecelerationWalking;
	initialWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;
	initialCrouchSpeed = GetCharacterMovement()->MaxWalkSpeedCrouched;
	GetCharacterMovement()->SetPlaneConstraintEnabled(true);
}

void AProtagonist::ResetMovementVariables()
{
	GetCharacterMovement()->GroundFriction = initialGroundFriction;
	GetCharacterMovement()->BrakingDecelerationWalking = initialBrakeDeceleration;
	GetCharacterMovement()->MaxWalkSpeed = initialWalkSpeed;
	GetCharacterMovement()->MaxWalkSpeedCrouched = initialCrouchSpeed;
	GetCharacterMovement()->SetPlaneConstraintEnabled(false);
}

void AProtagonist::SetSlidingVariables()
{
	bIsSliding=true;
	GetCharacterMovement()->GroundFriction = 0.0f;
	GetCharacterMovement()->BrakingDecelerationWalking = 1400.0f;
	GetCharacterMovement()->MaxWalkSpeedCrouched = 0.0f;
	GetCharacterMovement()->SetPlaneConstraintFromVectors(GetCharacterMovement()->Velocity.GetSafeNormal(), GetActorUpVector());
	GetCharacterMovement()->SetPlaneConstraintEnabled(true);
}

void AProtagonist::CheckSlideStart()
{
	
	if(canSlide && GetCharacterMovement()->Velocity.Size() >= 35.0f)
	{
		SetSlidingVariables();
		FVector impulseVector = GetActorForwardVector() * slideForce;
		GetCharacterMovement()->AddImpulse(impulseVector, true);
	}
}

void AProtagonist::SlideUpdate()
{
	if(bIsSliding)
	{
		if(!bIsPlayerCrouched || GetCharacterMovement()->IsFalling() || GetCharacterMovement()->Velocity.Size() <= 35.0f)
		{
			bIsSliding=false;
			ResetMovementVariables();
		}
	}
}


void AProtagonist::TurnRate(float value) {
	
	AddControllerYawInput(value * GetWorld()->GetDeltaSeconds() * turnRate);
}

void AProtagonist::LookUpRate(float value) {

	AddControllerPitchInput(value * GetWorld()->GetDeltaSeconds() * lookUpRate);
}


