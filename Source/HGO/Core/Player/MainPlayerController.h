// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HGO_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected: 
	bool _bHasInitializedInput = false;

	virtual void SetupInputComponent() override;

	void MoveRight(float value);
	void MoveForward(float value);

};
