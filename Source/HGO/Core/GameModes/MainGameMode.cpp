// Fill out your copyright notice in the Description page of Project Settings.


#include "MainGameMode.h"
void AMainGameMode::BeginPlay()
{
	Super::BeginPlay();
	timer = 0;
}

void AMainGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if(bHasGameEnded)
		return;

	
	timer += DeltaSeconds;
}


void AMainGameMode::EndGame()
{
	bHasGameEnded=true;
}
	