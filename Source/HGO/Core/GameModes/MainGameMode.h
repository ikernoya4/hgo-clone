// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainGameMode.generated.h"


UCLASS()
class HGO_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly) 
	float timer=0;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite);
	bool hasTimeSlowedDown=false;
	
	bool bHasGameEnded=false;
	
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	UFUNCTION(BlueprintCallable, Category = "GameFlow")
	void EndGame();
	
};
